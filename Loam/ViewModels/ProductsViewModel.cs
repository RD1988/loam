﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using Umbraco.Web.Composing;
using Umbraco.Web.Models;

namespace Loam.ViewModels
{
    public class ProductsViewModel : ContentModel
    {
        //public ProductsViewModel(IPublishedContent content) : base(content)
        //{

        //}

        public ProductsViewModel() : base(Current.UmbracoContext.PublishedRequest.PublishedContent)

        {
            Products = new List<ProductViewModel>();
        }

        public IList<ProductViewModel> Products;
    }
}