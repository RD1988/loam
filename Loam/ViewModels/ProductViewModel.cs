﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using Umbraco.Web.Composing;
using Umbraco.Web.Models;

namespace Loam.ViewModels
{
    public class ProductViewModel : ContentModel
    {
        public ProductViewModel(IPublishedContent content) : base(content)
        {

        }

        public ProductViewModel() : base(Current.UmbracoContext.PublishedRequest.PublishedContent)
       
        {
            Products = new List<ProductViewModel>();
            Variants = new List<ProductViewModel>();
            Properties = new List<ProductPropertiesViewModel>();
        }

        public bool IsVariant { get; set; }

        public bool IsProductFamily { get; set; }
        public IList<ProductViewModel> Products { get; set; }

        public string Tax { get; set; }
        public bool IsOrderingAllowed { get; set; }
     
        public string ThumbnailImageUrl { get; set; }
        public string VariantSku { get; set; }
        public string Sku { get; set; }
        public string PrimaryImageMediaId { get; set; }
       
        public IList<ProductViewModel> Variants { get; set; }

        public IList<ProductPropertiesViewModel> Properties { get; set; }


     

        public string LongDescription { get; set; }
        public string ShortDescription { get; set; }
        public string Url { get; set; }
        public string Name { get; set; }
   

        public string Price { get; set; }
        public double? Rating { get; set; }
    }
}