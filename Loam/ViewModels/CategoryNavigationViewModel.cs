﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loam.ViewModels
{
    public class CategoryNavigationViewModel
    {
        public CategoryNavigationViewModel()
        {
            Categories = new List<CategoryViewModel>();
        }

        public IEnumerable<CategoryViewModel> Categories { get; set; }
    }
}