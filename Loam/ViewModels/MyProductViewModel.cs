﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ucommerce.EntitiesV2;
using Umbraco.Core.Models.PublishedContent;

namespace Loam.ViewModels
{
    public class MyProductViewModel : PublishedContentWrapped
    {
        // The PublishedContentWrapped accepts an IPublishedContent item as a constructor
        public MyProductViewModel(IPublishedContent content) : base(content) 
        {
  
        }

        // Custom properties here...
        public int StockLevel = 1;

        public string Test { get; set; }


        public IList<Category> Categories { get; set; }




    }

}