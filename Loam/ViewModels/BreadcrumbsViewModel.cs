﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Loam.ViewModels
{
    public class BreadcrumbsViewModel
    {
        public string BreadcrumbName { get; set; }
        public string BreadcrumbUrl { get; set; }
    }
}