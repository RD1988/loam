﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using Umbraco.Web.Composing;
using Umbraco.Web.Models;
namespace Loam.ViewModels
{
    public class ProductPropertiesViewModel : PublishedContentWrapped
    {
        public ProductPropertiesViewModel(IPublishedContent content) : base(content)
        {

        }

        public ProductPropertiesViewModel() : base(Current.UmbracoContext.PublishedRequest.PublishedContent)
        {

        }

        public string PropertyName { get; set; }
        public IList<string> Values { get; set; }
    }
}