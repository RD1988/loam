﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ucommerce.Search.Definitions;
using Ucommerce.Search.Extensions;

namespace Loam.Search
{
    public class LoamProductIndexDefinition : DefaultProductsIndexDefinition
    {
        public LoamProductIndexDefinition() : base()
        {
            this.Field(p => p["ShowOnHomepage"], typeof(bool));
            this.Field(p => p["FemaleOrMale"], typeof(bool));
        }
    }
}