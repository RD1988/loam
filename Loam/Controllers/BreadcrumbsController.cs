﻿using Loam.ViewModels;
using System.Collections.Generic;
using System.Web.Mvc;
using Ucommerce.Catalog;
using Ucommerce.EntitiesV2;
using Ucommerce.Infrastructure;
using Umbraco.Web.Mvc;

namespace Loam.Controllers
{
    public class BreadcrumbsController : SurfaceController
    {
        public IUrlService UrlService => ObjectFactory.Instance.Resolve<IUrlService>();

        public ActionResult Index()
        {

            string getProduct = Request.QueryString["product"];
            Product product = Product.SingleOrDefault(z => z.Name == getProduct);
      
            IList<BreadcrumbsViewModel> breadcrumbs = new List<BreadcrumbsViewModel>();
            ProductCatalog catalog = ProductCatalog.SingleOrDefault(x => x.Name == "Loam");
            Category lastCategory = null;

            foreach (var categories in product.CategoryProductRelations)
            {
               
                string categoryName = categories.Category.Name;

                lastCategory = Category.SingleOrDefault(x => x.Name == categoryName);

                string categoryURL= UrlService.GetUrl(catalog, lastCategory);

                var breadcrum = new BreadcrumbsViewModel 
                {
                    BreadcrumbName = categories.Category.Name ,
                    BreadcrumbUrl = categoryURL
  
                };

                breadcrumbs.Add(breadcrum);
        
            }

            if (product != null)
            {
                string catalogName = Request.QueryString["catalog"];
                 //Response.Write(catalog);

                var breadcrumb = new BreadcrumbsViewModel
                {
                    BreadcrumbName = product.Name,
                    BreadcrumbUrl = catalogName


                };
                breadcrumbs.Add(breadcrumb);

            }



            return View("/Views/Partials/Breadcrumbs.cshtml", breadcrumbs);
        }
    }
}