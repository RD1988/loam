﻿using System;
using System.Web.Mvc;
using Ucommerce.Api;
using Ucommerce.Api.PriceCalculation;
using Ucommerce.Infrastructure;

using Ucommerce;
using Ucommerce.Search;
using Ucommerce.Search.Models;
using Ucommerce.Search.Slugs;
using Umbraco.Web.Mvc;
using System.Collections.Generic;
using Loam.ViewModels;
using System.Linq;

namespace Loam.Controllers
{
    public class HomeCatalogController : SurfaceController
    {

        public IUrlService UrlService => ObjectFactory.Instance.Resolve<IUrlService>();
   
        public ICatalogContext CatalogContext => ObjectFactory.Instance.Resolve<ICatalogContext>();
        public IIndex<Product> ProductIndex => ObjectFactory.Instance.Resolve<IIndex<Product>>();


        // GET: HomeCatalog
        public ActionResult Index()
        {

            var gender = Request.QueryString["category"];

            List<ProductViewModel> list = new List<ProductViewModel>();
            ProductsViewModel productsViewModel = new ProductsViewModel();


            if (gender == "kvinde")
            {
                var products = ProductIndex.Find()
             .Where(p => (bool)p["FemaleOrMale"] == false)
             .Take(12)
             .ToList();



                foreach (var product in products)
                {
                    string niceUrl = UrlService.GetUrl(CatalogContext.CurrentCatalog, product);



                    productsViewModel.Products.Add(new ProductViewModel
                    {
                        Name = product.Name,
                        ThumbnailImageUrl = product.ThumbnailImageUrl,
                        ShortDescription = product.ShortDescription,
                        Url = niceUrl
                    });

                };



                return View("/Views/Partials/HomeCatalog.cshtml", productsViewModel);
            }

            else if (gender == "mand")
            {
                var products = ProductIndex.Find().Where(p => (bool)p["FemaleOrMale"] == true).Take(12).ToList();




                foreach (var product in products)
                {
                    string niceUrl = UrlService.GetUrl(CatalogContext.CurrentCatalog, product);



                    productsViewModel.Products.Add(new ProductViewModel
                    {
                        Name = product.Name,
                        ThumbnailImageUrl = product.ThumbnailImageUrl,
                        ShortDescription = product.ShortDescription,
                        Url = niceUrl
                    });



                };



                return View("/Views/Partials/HomeCatalog.cshtml", productsViewModel);


            }


            else {
                var products = ProductIndex.Find()
                .Where(p => (bool)p["ShowOnHomepage"] == true)
                .Take(12)
                .ToList();



                foreach (var product in products)
                {
                    string niceUrl = UrlService.GetUrl(CatalogContext.CurrentCatalog, product);



                    productsViewModel.Products.Add(new ProductViewModel
                    {
                        Name = product.Name,
                        ThumbnailImageUrl = product.ThumbnailImageUrl,
                        ShortDescription = product.ShortDescription,
                        Url = niceUrl
                    });

                };

                return View("/Views/Partials/HomeCatalog.cshtml", productsViewModel);
            }



   


        }





    }
}