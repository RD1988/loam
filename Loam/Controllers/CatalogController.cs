﻿using Loam.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ucommerce.Api;
using Ucommerce.Catalog;
using Ucommerce.EntitiesV2;
using Ucommerce.Infrastructure;
using Ucommerce.Infrastructure.Logging;
using Umbraco.Web.Models;

namespace Loam.Controllers
{
    public class CatalogController : Umbraco.Web.Mvc.RenderMvcController
    {

        public override ActionResult Index(ContentModel model)
        {
           
            string category = Request.QueryString["category"];


            var query = from prod in Product.All() select prod;

            List<ProductViewModel> list = new List<ProductViewModel>();

            foreach (var product in query)
            {
                foreach (var categories in product.CategoryProductRelations.Where(x => x.Category.Name == category))
                {
                    foreach (var products in categories.Product.ProductDescriptions)
                    {
                        IUrlService urlService = ObjectFactory.Instance.Resolve<IUrlService>();

                        ProductCatalog catalog = ProductCatalog.SingleOrDefault(x => x.Name == "Loam");

                        Category category1 = Category.SingleOrDefault(x => x.Name == category);
                   
                        Product product1 = Product.SingleOrDefault(x => x.Name == products.DisplayName);
                    
                        string productUrl = urlService.GetUrl(catalog, category1, product1);
                
                       var productImage = Umbraco.Media(product.PrimaryImageMediaId);

                        if (productImage != null)
                        {
                            list.Add(new ProductViewModel { Name = product.Name, ThumbnailImageUrl = productImage.Url, ShortDescription = products.ShortDescription, Url = productUrl });
                        }


                    }
                   
                }
            
                
            }

      

            return PartialView("ProductsView", list);

       

        }

      


    }
}