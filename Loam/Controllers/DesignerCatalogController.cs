﻿using System.Web.Mvc;
using Ucommerce.Api;
using Ucommerce.Infrastructure;
using Ucommerce.Search;
using Ucommerce.Search.Models;
using Umbraco.Web.Mvc;
using System.Collections.Generic;
using Loam.ViewModels;
using System.Linq;



namespace Loam.Controllers
{
    public class DesignerCatalogController : SurfaceController
    {

        public ICatalogLibrary CatalogLibrary => ObjectFactory.Instance.Resolve<ICatalogLibrary>();
        public IIndex<Product> ProductIndex => ObjectFactory.Instance.Resolve<IIndex<Product>>();

        // GET: DesignerCatalog
        public ActionResult Index()
        {
            var categoryViewModel = new CategoryViewModel();
            List<Category> rootCategories = CatalogLibrary.GetRootCategories().ToList();
            categoryViewModel.Categories = MapCategories(rootCategories);
            return View("/Views/Partials/DesignerCatalogChoose.cshtml", categoryViewModel);
        }

        public ActionResult ShowMotives()
        {
         
            var mediaFolder = Umbraco.Media(1114);

            var list = new List<ProductViewModel>();

            foreach (var item in mediaFolder.Children)
            {
         
                list.Add(new ProductViewModel { PrimaryImageMediaId = item.Url });
                
                //Response.Write(item.Url); // item.url
            }



            return View("/Views/Partials/DesignerCatalogMotives.cshtml", list as IEnumerable<ProductViewModel>);
        }

        public ActionResult ShowProduct() 
        {
            var getcolor = Request.QueryString["color"];
            var productViewModel = new ProductViewModel();



            if (getcolor != null)
            {

                var product = ProductIndex.Find().Where(x => x.Name == getcolor).SingleOrDefault();
                productViewModel.PrimaryImageMediaId = product.PrimaryImageUrl;
                return View("/Views/Partials/DesignerCatalogShow.cshtml", productViewModel);
                //Response.Write(product.ThumbnailImageUrl);
            }

            else {

                var products = ProductIndex.Find().Where(x => x.Name == "Blue").SingleOrDefault();
                productViewModel.PrimaryImageMediaId = products.PrimaryImageUrl;
                //Response.Write(products.ThumbnailImageUrl);
                
                 return View("/Views/Partials/DesignerCatalogShow.cshtml", productViewModel);
            }

             
           // return View("/Views/Partials/DesignerCatalogShow.cshtml");
        }

    

        private IList<CategoryViewModel> MapCategories(List<Category> categoriesToMap)
        {
            var categoriesToReturn = new List<CategoryViewModel>();
            var allSubCategoryIds = categoriesToMap.SelectMany(cat => cat.Categories).Distinct().ToList();
            var subCategoriesById = CatalogLibrary.GetCategories(allSubCategoryIds).ToDictionary(cat => cat.Guid);

            foreach (var category in categoriesToMap)
            {

                foreach (var cat in category.Categories
              .Where(id => subCategoriesById.ContainsKey(id))
              .Select(id => subCategoriesById[id])
              .Select(cat => cat.Guid))
                {
                    var productsFromCategories = CatalogLibrary.GetProducts(cat);

                    foreach (var products in productsFromCategories)
                    {
                       
                        var categoryViewModel = new CategoryViewModel
                        {
                            Name = products.Name
                        };

                        categoriesToReturn.Add(categoryViewModel);
                    }

                };

            }

            return categoriesToReturn;
        }

        
    }
}