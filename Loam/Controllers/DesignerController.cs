﻿using Loam.ViewModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ucommerce.Api;
using Ucommerce.Catalog;
using Ucommerce.EntitiesV2;
using Ucommerce.Infrastructure;
using Ucommerce.Infrastructure.Logging;
using Ucommerce.Search;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace Loam.Controllers
{
    public class DesignerController : RenderMvcController
    {
      
        // GET: Designer
        public ActionResult Index(ContentModel model, string color)
        {
                

            //To do, render a macro in designer thats loads all the products..
            return View("/views/designer.cshtml", model.Content);
        }

        [HttpGet]
        public ActionResult Index()
        {
            string color = Request.QueryString["color"];


            Product product = Product.SingleOrDefault(z => z.Name == color);
            if (product != null)
            {
                //Response.Write(product.Name);
            }



            return View("/views/designer.cshtml");
        }


    }
}