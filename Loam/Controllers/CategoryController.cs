﻿using Loam.ViewModels;
using System.Collections.Generic;
using System.Web.Mvc;
using Ucommerce.Api;
using Ucommerce.Catalog;
using Ucommerce.EntitiesV2;
using Ucommerce.Infrastructure;


using Umbraco.Web.Mvc;


namespace Loam.Controllers
{
    public class CategoryController : SurfaceController
    {
        public ICatalogLibrary CatalogLibrary => ObjectFactory.Instance.Resolve<ICatalogLibrary>();
     

 
        public ActionResult Index()
        {

                var rootCategories = CatalogLibrary.GetRootCategories();
            
                List<CategoryViewModel> list = new List<CategoryViewModel>();

            


            foreach (var categories in rootCategories)
                {
  
                IUrlService urlService = ObjectFactory.Instance.Resolve<IUrlService>();

                ProductCatalog catalog = ProductCatalog.SingleOrDefault(x => x.Name == "Loam");
          

                Category category1 = Category.FirstOrDefault(x => x.Name == categories.Name);

                string productUrl = urlService.GetUrl(catalog, category1);

                list.Add(new CategoryViewModel { Name = categories.Name, Url = productUrl });
            
                };

                return PartialView("CategoryNavigation", list as IEnumerable<CategoryViewModel>);
    
        }

     
    }
}