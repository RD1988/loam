﻿
using System.Web.Mvc;


using Umbraco.Web.Mvc;

using Umbraco.Web.Models;


namespace Loam.Controllers
{
    
    public class HomeController : RenderMvcController
    {

        public  ActionResult Index(ContentModel model, string category)
        {
            //https://localhost:44354/?category=herre
          
            return View("/views/home.cshtml", model.Content);
        }



 
    }
}