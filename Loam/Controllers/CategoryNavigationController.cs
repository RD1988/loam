﻿using Loam.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Ucommerce.Api;
using Ucommerce.Catalog;
using Ucommerce.Infrastructure;
using Ucommerce.Search.Models;
using Umbraco.Web.Mvc;

namespace Loam.Controllers
{
    public class CategoryNavigationController : SurfaceController
    {
        public IUrlService UrlService => ObjectFactory.Instance.Resolve<IUrlService>();
        public ICatalogLibrary CatalogLibrary => ObjectFactory.Instance.Resolve<ICatalogLibrary>();
        public ICatalogContext CatalogContext => ObjectFactory.Instance.Resolve<ICatalogContext>();

        public ActionResult Index()
        {
            var categoryViewModel = new CategoryViewModel();

            List<Category> rootCategories = CatalogLibrary.GetRootCategories().ToList();

            categoryViewModel.Categories = MapCategories(rootCategories);

            return View(categoryViewModel);
           // return PartialView("Navigation", categoryViewModel as IEnumerable<CategoryViewModel>);
        }


        private IList<CategoryViewModel> MapCategories(IEnumerable<Category> categoriesToMap)
        {
            // Response.Write(categoriesToMap);
            var categoriesToReturn = new List<CategoryViewModel>();
            var allSubCategoryIds = categoriesToMap.SelectMany(cat => cat.Categories).Distinct().ToList();
            var subCategoriesById = CatalogLibrary.GetCategories(allSubCategoryIds).ToDictionary(cat => cat.Guid);

            foreach (var category in categoriesToMap)
            {
                Response.Write(category.DisplayName);
                var categoryViewModel = new CategoryViewModel
                {
                    Name = category.DisplayName,
                    //Url = _urlService.GetUrl(_catalogContext.CurrentCatalog,
                    //    _catalogContext.CurrentCategories.Union(new[] { category }))
                };
                categoryViewModel.Categories = category.Categories
                    .Where(id => subCategoriesById.ContainsKey(id))
                    .Select(id => subCategoriesById[id])
                    .Select(cat => new CategoryViewModel
                    {
                        Name = "sddsds",
                        //Url = _urlService.GetUrl(_catalogContext.CurrentCatalog, new[] { category, cat })
                    })
                    .ToList();

                categoriesToReturn.Add(categoryViewModel);
            }





            return categoriesToReturn;
        }




    }
}
    