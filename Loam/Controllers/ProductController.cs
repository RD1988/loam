﻿using Loam.ViewModels;

using System.Collections.Generic;

using System.Web.Mvc;
using Ucommerce.Api;
using Ucommerce.Catalog;
using Ucommerce.EntitiesV2;
using Ucommerce.Infrastructure;
using Ucommerce.Infrastructure.Logging;

using Umbraco.Web.Models;

namespace Loam.Controllers
{
    public class ProductController : Umbraco.Web.Mvc.RenderMvcController
    {

     
        // GET: ProductPage
        public override ActionResult Index(ContentModel model)
        {
            var transactionLibrary = ObjectFactory.Instance.Resolve<ITransactionLibrary>();

            string getProduct = Request.QueryString["product"];

            //get curret product
            Product product = Product.SingleOrDefault(z => z.Name == getProduct);

            //Init a view model
            var productViewModel = new ProductViewModel(model.Content);


            
            var productImage = Umbraco.Media(product.PrimaryImageMediaId);

            productViewModel.Name = product.Name;

            productViewModel.PrimaryImageMediaId = productImage.Url;

            //the MapVariants is a method for my Ilist variants
            productViewModel.Variants = MapVariants(product.Variants);
            //
            productViewModel.IsVariant = false;
        
          


            //Response.Write(MapProductProperties(product));
            productViewModel.Properties = MapProductProperties(product);

            //foreach (var props in product.Variants)
            //{
            //    foreach (var ProductDefinition in props.ProductProperties)
            //    {
            //        Response.Write(ProductDefinition.Value);
            //    }
            //}



            foreach (var descriptions in product.ProductDescriptions)
            {
                productViewModel.LongDescription = descriptions.LongDescription;
                    
            }

            foreach (var prices in product.ProductPrices)
            {
               
               
                foreach (var price in prices.Product.ProductPrices)
                {
                   
              
                    var formattedPrice = transactionLibrary.FormatCurrency(price.Price.Amount, "da-DK");
                    productViewModel.Price = formattedPrice;

              
                }
            }




            return CurrentTemplate(productViewModel);
        }

        private IList<ProductPropertiesViewModel> MapProductProperties(Product product)
        {
            var productProperties = new List<ProductPropertiesViewModel>();

      
            foreach (var prod in product.Variants)
            {

                foreach (var prop in prod.ProductProperties)
                {
                   
                


                    string[] value = { prop.Value };

                    productProperties.Add(new ProductPropertiesViewModel { PropertyName = prop.ProductDefinitionField.Name, Values = value });

                

                }



            }

           

            return productProperties;
     
        }
 
        private IList<ProductViewModel> MapVariants(IEnumerable<Product> product)
        {
           
            var variantModels = new List<ProductViewModel>();



            foreach (var currentVariant in product)
            {
                var variantImage = Umbraco.Media(currentVariant.PrimaryImageMediaId);



                if (variantImage != null)
                {
             
                
                    var productModel = new ProductViewModel
                    {
                        Name = currentVariant.Name,

                        PrimaryImageMediaId = variantImage.Url,
                        IsVariant = true

                    };

                    variantModels.Add(productModel);
                }

                
            }

         
            return variantModels;
        }


    



    }

}