﻿using System.Web.Mvc;
using System.Web.Routing;
using Umbraco.Core.Composing;
using Umbraco.Web;
using Umbraco.Web.Mvc;


namespace Loam.App_Code
{
    public class SetDefaultRenderMvcControllerComposer : IUserComposer
    {
        public void Compose(Composition composition)
        {
            // Custom route to MyProductController which will use a node with a specific ID as the
            // IPublishedContent for the current rendering page
            RouteTable.Routes.MapUmbracoRoute(
                "EventCustomRoute",
                "Arrangementer/{​​​​category}​​​​/{​​​​eventname}​​​​",
                new
                {
                    controller = "Events",
                    action = "index",
                    category = UrlParameter.Optional,
                    eventname = UrlParameter.Optional
                },
                new UmbracoVirtualNodeByIdRouteHandler(2665));
        }
    }
}