﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Umbraco.Core.Composing;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace Loam.
{

    public class RegisterCustomRouteComposer : ComponentComposer<RegisterCustomRouteComponent>
    {

    }

    public class RegisterCustomRouteComponent : IComponent
    {
        public void Initialize()
        {
            routes.MapRoute(
             name: "Default",
             url: "{controller}/{action}/{id}",
             defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
         );
        }

        public void Terminate()
        {
            throw new NotImplementedException();
        }
    }
}